--
-- Script de creation des tables
-- Auteurs
-- Code permanent: Steve Boucher    (BOUS30097608)
-- Code permanent: Maxime Girard    (GIRM30058500)
-- Code permanent: Mathieu Tanguay  (TANM28028902)
-- Code permanent: Dominic Turgeon  (TURD14058501)
-- 
--
SET ECHO ON

--parametre de sessions
--cette partie la n'estait pas dans le template du prof, c'est moi qui la ajoute
--on pourra le dropper a la fin du tp
--Steve B
SET SERVEROUTPUT ON
ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'
/
 
--Prenez note qu'il faudra surement reviser l'ordre des drops lorsque l'on mettre les secondary key en place
--dans le fichier de creation

DROP TABLE Emplacement;
DROP TABLE OeuvreAuteur;
DROP TABLE MusiqueCompositeur;
DROP TABLE MusiqueInterprete;
DROP TABLE FilmActeur;
DROP TABLE Compositeur;
DROP TABLE Interprete;
DROP TABLE Acteur;
DROP TABLE Musique;
DROP TABLE Film;
DROP TABLE Livre;
DROP TABLE Reservation;
DROP TABLE Pret;
DROP TABLE Exemplaire;
DROP TABLE Auteur;
DROP TABLE Oeuvre;
DROP TABLE Adherent;
DROP TABLE Personne;
DROP TABLE Etat_exemplaire;
DROP TABLE Support;
DROP TABLE Categorie;

SET ECHO OFF
