--
-- Script de remplissage
-- OeuvreAuteurs
-- Code permanent: Steve Boucher    (BOUS30097608)
-- Code permanent: Maxime Girard    (GIRM30058500)
-- Code permanent: Mathieu Tanguay  (TANM28028902)
-- Code permanent: Dominic Turgeon  (TURD14058501)
--
SET LINESIZE 160
SET ECHO ON
-----------------------------------------------------
-- CATEGORIES
-----------------------------------------------------
-- Livres, tiré des catégories de archambault.ca
INSERT INTO Categorie VALUES (1, 'livre', 'Arts');
INSERT INTO Categorie VALUES (2, 'livre', 'Biographie');
INSERT INTO Categorie VALUES (3, 'livre', 'Cuisine');
INSERT INTO Categorie VALUES (4, 'livre', 'Droit');
INSERT INTO Categorie VALUES (5, 'livre', 'Gestion');
INSERT INTO Categorie VALUES (6, 'livre', 'Géographie');
INSERT INTO Categorie VALUES (7, 'livre', 'Politique');
INSERT INTO Categorie VALUES (8, 'livre', 'Religion');
INSERT INTO Categorie VALUES (9, 'livre', 'Roman aventure');
INSERT INTO Categorie VALUES (10, 'livre', 'Roman fantastique');
INSERT INTO Categorie VALUES (11, 'livre', 'Roman historique');
INSERT INTO Categorie VALUES (12, 'livre', 'Roman horeur');
INSERT INTO Categorie VALUES (13, 'livre', 'Roman jeunesse');
INSERT INTO Categorie VALUES (14, 'livre', 'Roman québécois');
INSERT INTO Categorie VALUES (15, 'livre', 'Roman romantique');
INSERT INTO Categorie VALUES (16, 'livre', 'Roman science-fiction');
INSERT INTO Categorie VALUES (17, 'livre', 'Sexologie');
INSERT INTO Categorie VALUES (18, 'livre', 'Sociologie');
INSERT INTO Categorie VALUES (19, 'livre', 'Théatre');
INSERT INTO Categorie VALUES (20, 'livre', 'Voyage');
INSERT INTO Categorie VALUES (21, 'livre', 'Économie');
INSERT INTO Categorie VALUES (22, 'livre', 'Ésotérisme');

-- Film, tiré des catégories du Super Club Vidéotron
INSERT INTO Categorie VALUES (23, 'film', 'Action');
INSERT INTO Categorie VALUES (24, 'film', 'Amour');
INSERT INTO Categorie VALUES (25, 'film', 'Animation');
INSERT INTO Categorie VALUES (26, 'film', 'Arts martiaux');
INSERT INTO Categorie VALUES (27, 'film', 'Aventure');
INSERT INTO Categorie VALUES (28, 'film', 'Biographie');
INSERT INTO Categorie VALUES (29, 'film', 'Classique');
INSERT INTO Categorie VALUES (30, 'film', 'Comédie dramatique');
INSERT INTO Categorie VALUES (31, 'film', 'Comédie musicale');
INSERT INTO Categorie VALUES (32, 'film', 'Comédie');
INSERT INTO Categorie VALUES (33, 'film', 'Documentaire');
INSERT INTO Categorie VALUES (34, 'film', 'Drame');
INSERT INTO Categorie VALUES (35, 'film', 'Enfants');
INSERT INTO Categorie VALUES (36, 'film', 'Famille');
INSERT INTO Categorie VALUES (37, 'film', 'Fantastique');
INSERT INTO Categorie VALUES (38, 'film', 'France');
INSERT INTO Categorie VALUES (39, 'film', 'Guerre');
INSERT INTO Categorie VALUES (40, 'film', 'Horreur');
INSERT INTO Categorie VALUES (41, 'film', 'Humour');
INSERT INTO Categorie VALUES (42, 'film', 'International');
INSERT INTO Categorie VALUES (43, 'film', 'Musique');
INSERT INTO Categorie VALUES (44, 'film', 'Policier et Crime');
INSERT INTO Categorie VALUES (45, 'film', 'Québec');
INSERT INTO Categorie VALUES (46, 'film', 'Science-fiction');
INSERT INTO Categorie VALUES (47, 'film', 'Sport');
INSERT INTO Categorie VALUES (48, 'film', 'Suspense');
INSERT INTO Categorie VALUES (49, 'film', 'Série télé');
INSERT INTO Categorie VALUES (50, 'film', 'Western');

-- Musique, tiré des catégories sur archambault.ca
INSERT INTO Categorie VALUES (51, 'musique', 'Blues');
INSERT INTO Categorie VALUES (52, 'musique', 'Country');
INSERT INTO Categorie VALUES (53, 'musique', 'Folklorique');
INSERT INTO Categorie VALUES (54, 'musique', 'Francophone');
INSERT INTO Categorie VALUES (55, 'musique', 'Gospel');
INSERT INTO Categorie VALUES (56, 'musique', 'Hip hop');
INSERT INTO Categorie VALUES (57, 'musique', 'Instrumental');
INSERT INTO Categorie VALUES (58, 'musique', 'Jazz');
INSERT INTO Categorie VALUES (59, 'musique', 'Karaoké');
INSERT INTO Categorie VALUES (60, 'musique', 'Musique de film');
INSERT INTO Categorie VALUES (61, 'musique', 'Musique du monde (Jazz)');
INSERT INTO Categorie VALUES (62, 'musique', 'Musique du temps des fêtes');
INSERT INTO Categorie VALUES (63, 'musique', 'Métal');
INSERT INTO Categorie VALUES (64, 'musique', 'Nouvel âge');
INSERT INTO Categorie VALUES (65, 'musique', 'Opéra');
INSERT INTO Categorie VALUES (66, 'musique', 'Pop');
INSERT INTO Categorie VALUES (67, 'musique', 'Punk');
INSERT INTO Categorie VALUES (68, 'musique', 'Rock');
INSERT INTO Categorie VALUES (69, 'musique', 'Électronique');

-----------------------------------------------------
-- SUPPORT
-----------------------------------------------------
INSERT INTO Support VALUES (1,  'livre');
INSERT INTO Support VALUES (2,  'cassette audio');
INSERT INTO Support VALUES (3,  'cassette video');
INSERT INTO Support VALUES (4,  'cd');
INSERT INTO Support VALUES (5,  'dvd');

-----------------------------------------------------
-- ETAT_EXEMPLAIRE
-----------------------------------------------------
INSERT INTO Etat_exemplaire VALUES (1,  'Neuf');
INSERT INTO Etat_exemplaire VALUES (2,	'Comme neuf');
INSERT INTO Etat_exemplaire VALUES (3, 	'Très bonne condition');
INSERT INTO Etat_exemplaire VALUES (4, 	'Bonne condition');
INSERT INTO Etat_exemplaire VALUES (5, 	'Acceptable');
INSERT INTO Etat_exemplaire VALUES (6, 	'Défectueux');
INSERT INTO Etat_exemplaire VALUES (7,	'Irrécupérable');


-----------------------------------------------------
-- PERSONNE
-----------------------------------------------------
INSERT INTO Personne VALUES ('P001',  'Boucher', 'Steve');
INSERT INTO Personne VALUES ('P002',  'Girard', 'Maxime');
INSERT INTO Personne VALUES ('P003',  'Tanguay', 'Mathieu');
INSERT INTO Personne VALUES ('P004',  'Turgeon', 'Dominic');
INSERT INTO Personne VALUES ('P005',  'George', 'Lucas');
INSERT INTO Personne VALUES ('P006',  'Isaac', 'Assimov');
INSERT INTO Personne VALUES ('P007',  'Arthur C.', 'Clarke');
INSERT INTO Personne VALUES ('P008',  'Elvis', 'Presley');
INSERT INTO Personne VALUES ('P009',  'Oscar', 'Peterson');
INSERT INTO Personne VALUES ('P010',  'Musicien', 'XYZ');



-----------------------------------------------------
-- ADHERENT
-----------------------------------------------------
INSERT INTO Adherent VALUES (1, 'P001', '1 rue uqam',    	'555-1234', 'membre en regle');
INSERT INTO Adherent VALUES (2, 'P002', '2 rue sql',     	'555-4321', 'membre en regle');
INSERT INTO Adherent VALUES (3, 'P003', '3 rue tetris',  	'555-2222', 'membre en regle');
INSERT INTO Adherent VALUES (4, 'P004', '4 rue universite', '555-3333', 'membre en regle');


-----------------------------------------------------
-- OEUVRE
-----------------------------------------------------

-- Livres
INSERT INTO Oeuvre VALUES (1,  1, 'Seigneur des anneaux vol.1', 'V');
INSERT INTO Oeuvre VALUES (2,  1, 'Seigneur des anneaux vol.2', 'F');
INSERT INTO Oeuvre VALUES (3,  1, 'Seigneur des anneaux vol.3', 'F');
INSERT INTO Oeuvre VALUES (4,  2, 'Coup de pouce vol.1', 'F');
INSERT INTO Oeuvre VALUES (5,  2, 'Coup de pouce vol.2', 'F');
INSERT INTO Oeuvre VALUES (6,  2, 'Coup de pouce vol.3', 'F');

-- Films
INSERT INTO Oeuvre VALUES (7,  3, 'Star Wars I', 'F');
INSERT INTO Oeuvre VALUES (8,  3, 'Star Wars II', 'F');
INSERT INTO Oeuvre VALUES (9,  3, 'Star Wars III', 'F');
INSERT INTO Oeuvre VALUES (10, 4, 'U2 en show', 'F');
INSERT INTO Oeuvre VALUES (11, 4, 'Iron Maiden en show', 'F');
INSERT INTO Oeuvre VALUES (12, 4, 'Metallica en show', 'F');
INSERT INTO Oeuvre VALUES (13, 5, 'Decouverte ed.1', 'F');
INSERT INTO Oeuvre VALUES (14, 5, 'Decouverte ed.2', 'F');
INSERT INTO Oeuvre VALUES (15, 5, 'Decouverte ed.3', 'F');

--Musique
INSERT INTO Oeuvre VALUES (16, 6, 'Elvis album.1', 'F');
INSERT INTO Oeuvre VALUES (17, 6, 'Elvis album.2', 'F');
INSERT INTO Oeuvre VALUES (18, 6, 'Elvis album.3', 'F');
INSERT INTO Oeuvre VALUES (19, 7, 'Jazz album.1', 'F');
INSERT INTO Oeuvre VALUES (20, 7, 'Jazz album.2', 'F');
INSERT INTO Oeuvre VALUES (21, 7, 'Jazz album.3', 'F');
INSERT INTO Oeuvre VALUES (22, 8, 'Pop album.1', 'F');
INSERT INTO Oeuvre VALUES (23, 8, 'Pop album.2', 'F');
INSERT INTO Oeuvre VALUES (24, 8, 'Pop album.3', 'F');
INSERT INTO Oeuvre VALUES (25, 9, 'Boum boum album.1', 'F');
INSERT INTO Oeuvre VALUES (26, 9, 'Boum boum album.2', 'F');
INSERT INTO Oeuvre VALUES (27, 9, 'Boum boum album.3', 'F');

-- Tiré du Super Club Vidéotron
INSERT INTO Oeuvre VALUES (28,23, 'L''épreuve : Le labyrinthe', 'V');
INSERT INTO Oeuvre VALUES (29,23, 'Le Justicier', 'F');
INSERT INTO Oeuvre VALUES (30,23, 'Les tortues Ninja', 'F');
INSERT INTO Oeuvre VALUES (31,23, 'Maïna', 'F');
INSERT INTO Oeuvre VALUES (32,24, 'C''est pas si simple', 'F');
INSERT INTO Oeuvre VALUES (33,24, 'Dansez dans les rues 5', 'F');
INSERT INTO Oeuvre VALUES (34,24, 'Le spectacle de Noël', 'F');
INSERT INTO Oeuvre VALUES (35,24, 'The One I Love', 'F');
INSERT INTO Oeuvre VALUES (36,25, 'Dragons 2', 'F');
INSERT INTO Oeuvre VALUES (37,25, 'Family Guy - Volume 13', 'F');
INSERT INTO Oeuvre VALUES (38,25, 'Le livre dela jungle - Les singes s''amusent', 'F');
INSERT INTO Oeuvre VALUES (39,25, 'Les Simpsons - Saison 17', 'F');
INSERT INTO Oeuvre VALUES (40,26, 'Georges St-Pierre : L''ADN d''un champion', 'F');
INSERT INTO Oeuvre VALUES (41,26, 'Soumission', 'F');
INSERT INTO Oeuvre VALUES (42,26, 'The Protector 2', 'F');
INSERT INTO Oeuvre VALUES (43,26, 'The Raid 2', 'F');
INSERT INTO Oeuvre VALUES (44,27, 'Bigfoot', 'F');
INSERT INTO Oeuvre VALUES (45,27, 'Hercule', 'F');
INSERT INTO Oeuvre VALUES (46,27, 'Le secret de l''étoile du nord', 'F');
INSERT INTO Oeuvre VALUES (47,27, 'Les vieux qui revivaient leur jeunesse Islandaise', 'F');
INSERT INTO Oeuvre VALUES (48,28, 'Frank', 'F');
INSERT INTO Oeuvre VALUES (49,28, 'Jersey Boys', 'F');
INSERT INTO Oeuvre VALUES (50,28, 'La petite reine', 'F');
INSERT INTO Oeuvre VALUES (51,28, 'Yves Saint-laurent', 'F');
INSERT INTO Oeuvre VALUES (52,29, 'Halloween : Complete Collection', 'F');
INSERT INTO Oeuvre VALUES (53,29, 'La maison du diable', 'F');
INSERT INTO Oeuvre VALUES (54,29, 'Mry Poppins', 'F');
INSERT INTO Oeuvre VALUES (55,29, 'Sleepaway Camp', 'F');
INSERT INTO Oeuvre VALUES (56,30, 'Baby sitting', 'F');
INSERT INTO Oeuvre VALUES (57,30, 'Dr. Cabbie', 'F');
INSERT INTO Oeuvre VALUES (58,30, 'Swearnet: The Movie', 'F');
INSERT INTO Oeuvre VALUES (59,30, 'Une semaine sans fin', 'F');
INSERT INTO Oeuvre VALUES (60,31, 'Et si jamais', 'F');
INSERT INTO Oeuvre VALUES (61,31, 'Histoire de dauphin 2', 'F');
INSERT INTO Oeuvre VALUES (62,31, 'Le chant des anges', 'F');
INSERT INTO Oeuvre VALUES (63,31, 'Le voyage de cent pas', 'F');
INSERT INTO Oeuvre VALUES (64,32, 'Black Nativity', 'F');
INSERT INTO Oeuvre VALUES (65,32, 'Glee - Season 4', 'F');
INSERT INTO Oeuvre VALUES (66,32, 'La nouvelle Blanche-Neige', 'F');
INSERT INTO Oeuvre VALUES (67,32, 'Le magicien d''Oz', 'F');
INSERT INTO Oeuvre VALUES (68,33, 'Fermières', 'F');
INSERT INTO Oeuvre VALUES (69,33, 'For No Good Reason', 'F');
INSERT INTO Oeuvre VALUES (70,33, 'Jodorowsky''s Dune', 'F');
INSERT INTO Oeuvre VALUES (71,33, 'Made in America', 'F');
INSERT INTO Oeuvre VALUES (72,34, 'L''immigrante', 'F');
INSERT INTO Oeuvre VALUES (73,34, 'Le beau mensonge', 'F');
INSERT INTO Oeuvre VALUES (74,34, 'Le match d''après', 'F');
INSERT INTO Oeuvre VALUES (75,34, 'True Blood - Saison 7', 'F');
INSERT INTO Oeuvre VALUES (76,35, 'Au pays du miroir magique', 'F');
INSERT INTO Oeuvre VALUES (77,35, 'Les avions : Les pompiers du ciel', 'F');
INSERT INTO Oeuvre VALUES (78,35, 'M. Peabody et Sherman', 'F');
INSERT INTO Oeuvre VALUES (79,35, 'Princesse Sofia : Les fêtes à Enchancia', 'F');
INSERT INTO Oeuvre VALUES (80,36, 'La gang des hors-la-loi', 'F');
INSERT INTO Oeuvre VALUES (81,36, 'Les vacances du peteit Nicolas', 'F');
INSERT INTO Oeuvre VALUES (82,36, 'Maléfique', 'F');
INSERT INTO Oeuvre VALUES (83,36, 'Smitty le chien', 'F');
INSERT INTO Oeuvre VALUES (84,37, 'Journal d''un vampire - Saison 4', 'F');
INSERT INTO Oeuvre VALUES (85,37, 'Le hobbit : La désolation de Smaug - Édition prolongée', 'F');
INSERT INTO Oeuvre VALUES (86,37, 'Les gardiens de la galaxie', 'F');
INSERT INTO Oeuvre VALUES (87,37, 'Surnaturel - Saison 8', 'F');
INSERT INTO Oeuvre VALUES (88,38, 'Aimer, boire et chanter', 'F');
INSERT INTO Oeuvre VALUES (89,38, 'Le volcan', 'F');
INSERT INTO Oeuvre VALUES (90,38, 'Les apaches', 'F');
INSERT INTO Oeuvre VALUES (91,38, 'Qu''est-ce qu''on a fait au bon dieu?', 'F');
INSERT INTO Oeuvre VALUES (92,39, '300 : La naissance d''un empire', 'F');
INSERT INTO Oeuvre VALUES (93,39, 'Bunker', 'F');
INSERT INTO Oeuvre VALUES (94,39, 'Fort Bliss', 'F');
INSERT INTO Oeuvre VALUES (95,39, 'Jarhead 2 : Field of Fire', 'F');
INSERT INTO Oeuvre VALUES (96,40, 'Au-delà de la mort', 'F');
INSERT INTO Oeuvre VALUES (97,40, 'Là-haut comme ici-bas', 'F');
INSERT INTO Oeuvre VALUES (98,40, 'Neige mortelle 2', 'F');
INSERT INTO Oeuvre VALUES (99,40, 'The Strain - Season 1', 'F');
INSERT INTO Oeuvre VALUES (100,41, 'Billy Tellier : La loi du plus fort', 'F');
INSERT INTO Oeuvre VALUES (101,41, 'Jean-Marc Parent-Torture', 'F');
INSERT INTO Oeuvre VALUES (102,41, 'Juste pour rire : Juste le meilleur des galas 2014', 'F');
INSERT INTO Oeuvre VALUES (103,41, 'Marie-Lise Pilote - Réconfortante', 'F');
INSERT INTO Oeuvre VALUES (104,42, 'Barbecue', 'F');
INSERT INTO Oeuvre VALUES (105,42, 'La chute d''un empire', 'F');
INSERT INTO Oeuvre VALUES (106,42, 'Loup', 'F');
INSERT INTO Oeuvre VALUES (107,42, 'Pour une femme', 'F');
INSERT INTO Oeuvre VALUES (108,43, 'CBGB', 'F');
INSERT INTO Oeuvre VALUES (109,43, 'Justin Bieber''s Believe', 'F');
INSERT INTO Oeuvre VALUES (110,43, 'Nouveau refrain', 'F');
INSERT INTO Oeuvre VALUES (111,43, 'Super Duper Alice Cooper', 'F');
INSERT INTO Oeuvre VALUES (112,44, 'Brooklyn Nine-nine - Season 1', 'F');
INSERT INTO Oeuvre VALUES (113,44, 'Délivrez-nous du mal', 'F');
INSERT INTO Oeuvre VALUES (114,44, 'Le corrompu', 'F');
INSERT INTO Oeuvre VALUES (115,44, 'The Following - Saison 2', 'F');
INSERT INTO Oeuvre VALUES (116,45, '1987', 'F');
INSERT INTO Oeuvre VALUES (117,45, 'Le vrai du faux', 'F');
INSERT INTO Oeuvre VALUES (118,45, 'Les Boys : La série - Saison 5', 'F');
INSERT INTO Oeuvre VALUES (119,45, 'Peter Macleod - Sagesse reportée', 'F');
INSERT INTO Oeuvre VALUES (120,46, 'I Origins', 'F');
INSERT INTO Oeuvre VALUES (121,46, 'Le passeur', 'F');
INSERT INTO Oeuvre VALUES (122,46, 'Les dernières heures', 'F');
INSERT INTO Oeuvre VALUES (123,46, 'Lucy', 'F');
INSERT INTO Oeuvre VALUES (124,47, 'Banshee - Saison 2', 'F');
INSERT INTO Oeuvre VALUES (125,47, 'Dominion - Season 1', 'F');
INSERT INTO Oeuvre VALUES (126,47, 'Intruders - Season 1', 'F');
INSERT INTO Oeuvre VALUES (127,47, 'The Americans - Season 2', 'F');
INSERT INTO Oeuvre VALUES (128,48, 'Combat revanche', 'F');
INSERT INTO Oeuvre VALUES (129,48, 'Il était une fois les boys', 'F');
INSERT INTO Oeuvre VALUES (130,48, 'Le grand défi', 'F');
INSERT INTO Oeuvre VALUES (131,48, 'Le repêchage', 'F');
INSERT INTO Oeuvre VALUES (132,49, 'Capture en enfer', 'F');
INSERT INTO Oeuvre VALUES (133,49, 'Cornes', 'F');
INSERT INTO Oeuvre VALUES (134,49, 'Enlevée', 'F');
INSERT INTO Oeuvre VALUES (135,49, 'Uner de Dome - Season 2', 'F');
INSERT INTO Oeuvre VALUES (136,50, 'Cowboys vs. Zombies', 'F');
INSERT INTO Oeuvre VALUES (137,50, 'La bataille du rail - Saison 3', 'F');
INSERT INTO Oeuvre VALUES (138,50, 'Mille et une façons de mourir dans l''ouest', 'F');
INSERT INTO Oeuvre VALUES (139,50, 'Une nuit au Vieux-Mexique', 'F');

-- Autres Livres

INSERT INTO Oeuvre VALUES (140,  16, 'I, Robot', 'F');
INSERT INTO Oeuvre VALUES (141,  16, 'The Rest of the Robots', 'F');
INSERT INTO Oeuvre VALUES (142,  16, 'The Complete Robot', 'F');
INSERT INTO Oeuvre VALUES (143,  16, 'Robot Dreams', 'F');
INSERT INTO Oeuvre VALUES (144,  16, 'Robot Visions', 'F');
INSERT INTO Oeuvre VALUES (145,  16, 'The Positronic Man', 'F');
INSERT INTO Oeuvre VALUES (146,  16, 'The Sands of Mars ', 'F');
INSERT INTO Oeuvre VALUES (147,  16, 'Childhood''s End', 'F');
INSERT INTO Oeuvre VALUES (148,  16, 'The City and the Stars', 'F');
INSERT INTO Oeuvre VALUES (149,  16, 'The Deep Range ', 'F');
INSERT INTO Oeuvre VALUES (150,  16, 'A Fall of Moondust', 'F');
INSERT INTO Oeuvre VALUES (151,  16, '2001: A Space Odyssey', 'F');


-- Autre Musique

INSERT INTO Oeuvre VALUES (152, 6, 'Elvis album.4', 'F');
INSERT INTO Oeuvre VALUES (153, 6, 'Elvis album.5', 'F');
INSERT INTO Oeuvre VALUES (154, 6, 'Elvis album.6', 'F');
INSERT INTO Oeuvre VALUES (155, 7, 'Jazz album.4', 'F');
INSERT INTO Oeuvre VALUES (156, 7, 'Jazz album.5', 'F');
INSERT INTO Oeuvre VALUES (157, 7, 'Jazz album.6', 'F');
INSERT INTO Oeuvre VALUES (158, 8, 'Pop album.4', 'F');
INSERT INTO Oeuvre VALUES (159, 8, 'Pop album.5', 'F');
INSERT INTO Oeuvre VALUES (160, 8, 'Pop album.6', 'F');
INSERT INTO Oeuvre VALUES (161, 9, 'Boum boum album.4', 'F');
INSERT INTO Oeuvre VALUES (162, 9, 'Boum boum album.5', 'F');
INSERT INTO Oeuvre VALUES (163, 9, 'Boum boum album.6', 'F');

-----------------------------------------------------
-- AUTEUR
-----------------------------------------------------
INSERT INTO Auteur VALUES ('P006');
INSERT INTO Auteur VALUES ('P007');

-----------------------------------------------------
-- EXEMPLAIRE
-----------------------------------------------------
INSERT INTO Exemplaire VALUES (1,28,5,3);
INSERT INTO Exemplaire VALUES (2,29,5,3);
INSERT INTO Exemplaire VALUES (3,30,5,3);
INSERT INTO Exemplaire VALUES (4,31,5,3);
INSERT INTO Exemplaire VALUES (5,32,5,3);
INSERT INTO Exemplaire VALUES (6,33,5,3);
INSERT INTO Exemplaire VALUES (7,34,5,3);
INSERT INTO Exemplaire VALUES (8,35,5,3);
INSERT INTO Exemplaire VALUES (9,36,5,3);
INSERT INTO Exemplaire VALUES (10,37,5,3);
INSERT INTO Exemplaire VALUES (11,38,5,3);
INSERT INTO Exemplaire VALUES (12,39,5,3);
INSERT INTO Exemplaire VALUES (13,40,5,3);
INSERT INTO Exemplaire VALUES (14,41,5,3);
INSERT INTO Exemplaire VALUES (15,42,5,3);
INSERT INTO Exemplaire VALUES (16,43,5,3);
INSERT INTO Exemplaire VALUES (17,44,5,3);
INSERT INTO Exemplaire VALUES (18,45,5,3);
INSERT INTO Exemplaire VALUES (19,46,5,3);
INSERT INTO Exemplaire VALUES (20,47,5,3);
INSERT INTO Exemplaire VALUES (21,48,5,3);
INSERT INTO Exemplaire VALUES (22,49,5,3);
INSERT INTO Exemplaire VALUES (23,50,5,3);
INSERT INTO Exemplaire VALUES (24,51,5,3);
INSERT INTO Exemplaire VALUES (25,52,5,3);
INSERT INTO Exemplaire VALUES (26,53,5,3);
INSERT INTO Exemplaire VALUES (27,54,5,3);
INSERT INTO Exemplaire VALUES (28,55,5,3);
INSERT INTO Exemplaire VALUES (29,56,5,3);
INSERT INTO Exemplaire VALUES (30,57,5,3);
INSERT INTO Exemplaire VALUES (31,58,5,3);
INSERT INTO Exemplaire VALUES (32,59,5,3);
INSERT INTO Exemplaire VALUES (33,60,5,3);
INSERT INTO Exemplaire VALUES (34,61,5,3);
INSERT INTO Exemplaire VALUES (35,62,5,3);
INSERT INTO Exemplaire VALUES (36,63,5,3);
INSERT INTO Exemplaire VALUES (37,64,5,3);
INSERT INTO Exemplaire VALUES (38,65,5,3);
INSERT INTO Exemplaire VALUES (39,66,5,3);
INSERT INTO Exemplaire VALUES (40,67,5,3);
INSERT INTO Exemplaire VALUES (41,68,5,3);
INSERT INTO Exemplaire VALUES (42,69,5,3);
INSERT INTO Exemplaire VALUES (43,70,5,3);
INSERT INTO Exemplaire VALUES (44,71,5,3);
INSERT INTO Exemplaire VALUES (45,72,5,3);
INSERT INTO Exemplaire VALUES (46,73,5,3);
INSERT INTO Exemplaire VALUES (47,74,5,3);
INSERT INTO Exemplaire VALUES (48,75,5,3);
INSERT INTO Exemplaire VALUES (49,76,5,3);
INSERT INTO Exemplaire VALUES (50,77,5,3);
INSERT INTO Exemplaire VALUES (51,78,5,3);
INSERT INTO Exemplaire VALUES (52,79,5,3);
INSERT INTO Exemplaire VALUES (53,80,5,3);
INSERT INTO Exemplaire VALUES (54,81,5,3);
INSERT INTO Exemplaire VALUES (55,82,5,3);
INSERT INTO Exemplaire VALUES (56,83,5,3);
INSERT INTO Exemplaire VALUES (57,84,5,3);
INSERT INTO Exemplaire VALUES (58,85,5,3);
INSERT INTO Exemplaire VALUES (59,86,5,3);
INSERT INTO Exemplaire VALUES (60,87,5,3);
INSERT INTO Exemplaire VALUES (61,88,5,3);
INSERT INTO Exemplaire VALUES (62,89,5,3);
INSERT INTO Exemplaire VALUES (63,90,5,3);
INSERT INTO Exemplaire VALUES (64,91,5,3);
INSERT INTO Exemplaire VALUES (65,92,5,3);
INSERT INTO Exemplaire VALUES (66,93,5,3);
INSERT INTO Exemplaire VALUES (67,94,5,3);
INSERT INTO Exemplaire VALUES (68,95,5,3);
INSERT INTO Exemplaire VALUES (69,96,5,3);
INSERT INTO Exemplaire VALUES (70,97,5,3);
INSERT INTO Exemplaire VALUES (71,98,5,3);
INSERT INTO Exemplaire VALUES (72,99,5,3);
INSERT INTO Exemplaire VALUES (73,100,5,3);
INSERT INTO Exemplaire VALUES (74,101,5,3);
INSERT INTO Exemplaire VALUES (75,102,5,3);
INSERT INTO Exemplaire VALUES (76,103,5,3);
INSERT INTO Exemplaire VALUES (77,104,5,3);
INSERT INTO Exemplaire VALUES (78,105,5,3);
INSERT INTO Exemplaire VALUES (79,106,5,3);
INSERT INTO Exemplaire VALUES (80,107,5,3);
INSERT INTO Exemplaire VALUES (81,108,5,3);
INSERT INTO Exemplaire VALUES (82,109,5,3);
INSERT INTO Exemplaire VALUES (83,110,5,3);
INSERT INTO Exemplaire VALUES (84,111,5,3);
INSERT INTO Exemplaire VALUES (85,112,5,3);
INSERT INTO Exemplaire VALUES (86,113,5,3);
INSERT INTO Exemplaire VALUES (87,114,5,3);
INSERT INTO Exemplaire VALUES (88,115,5,3);
INSERT INTO Exemplaire VALUES (89,116,5,3);
INSERT INTO Exemplaire VALUES (90,117,5,3);
INSERT INTO Exemplaire VALUES (91,118,5,3);
INSERT INTO Exemplaire VALUES (92,119,5,3);
INSERT INTO Exemplaire VALUES (93,120,5,3);
INSERT INTO Exemplaire VALUES (94,121,5,3);
INSERT INTO Exemplaire VALUES (95,122,5,3);
INSERT INTO Exemplaire VALUES (96,123,5,3);
INSERT INTO Exemplaire VALUES (97,124,5,3);
INSERT INTO Exemplaire VALUES (98,125,5,3);
INSERT INTO Exemplaire VALUES (99,126,5,3);
INSERT INTO Exemplaire VALUES (100,127,5,3);
INSERT INTO Exemplaire VALUES (101,128,5,3);
INSERT INTO Exemplaire VALUES (102,129,5,3);
INSERT INTO Exemplaire VALUES (103,130,5,3);
INSERT INTO Exemplaire VALUES (104,131,5,3);
INSERT INTO Exemplaire VALUES (105,132,5,3);
INSERT INTO Exemplaire VALUES (106,133,5,3);
INSERT INTO Exemplaire VALUES (107,134,5,3);
INSERT INTO Exemplaire VALUES (108,135,5,3);
INSERT INTO Exemplaire VALUES (109,136,5,3);
INSERT INTO Exemplaire VALUES (110,137,5,3);
INSERT INTO Exemplaire VALUES (111,138,5,3);
INSERT INTO Exemplaire VALUES (112,139,5,3);

--Autres livres
INSERT INTO Exemplaire VALUES (113,140,1,6);
INSERT INTO Exemplaire VALUES (114,140,1,7);
INSERT INTO Exemplaire VALUES (115,141,1,6);
INSERT INTO Exemplaire VALUES (116,141,1,3);
INSERT INTO Exemplaire VALUES (117,142,1,3);
INSERT INTO Exemplaire VALUES (118,142,1,3);
INSERT INTO Exemplaire VALUES (119,143,1,3);
INSERT INTO Exemplaire VALUES (120,143,1,3);
INSERT INTO Exemplaire VALUES (121,144,1,3);
INSERT INTO Exemplaire VALUES (122,144,1,3);
INSERT INTO Exemplaire VALUES (123,145,1,3);
INSERT INTO Exemplaire VALUES (124,145,1,3);
INSERT INTO Exemplaire VALUES (125,146,1,3);
INSERT INTO Exemplaire VALUES (126,146,1,3);
INSERT INTO Exemplaire VALUES (127,147,1,3);
INSERT INTO Exemplaire VALUES (128,147,1,3);
INSERT INTO Exemplaire VALUES (129,148,1,3);
INSERT INTO Exemplaire VALUES (130,148,1,3);
INSERT INTO Exemplaire VALUES (131,149,1,3);
INSERT INTO Exemplaire VALUES (132,149,1,3);
INSERT INTO Exemplaire VALUES (133,150,1,3);
INSERT INTO Exemplaire VALUES (134,150,1,3);
INSERT INTO Exemplaire VALUES (135,151,1,3);
INSERT INTO Exemplaire VALUES (136,151,1,3);

--Autre Musique
INSERT INTO Exemplaire VALUES (137,152,4,3);
INSERT INTO Exemplaire VALUES (138,152,4,3);
INSERT INTO Exemplaire VALUES (139,153,4,3);
INSERT INTO Exemplaire VALUES (140,153,4,3);
INSERT INTO Exemplaire VALUES (141,154,4,3);
INSERT INTO Exemplaire VALUES (142,154,4,3);
INSERT INTO Exemplaire VALUES (143,155,4,3);
INSERT INTO Exemplaire VALUES (144,155,4,3);
INSERT INTO Exemplaire VALUES (145,156,4,3);
INSERT INTO Exemplaire VALUES (146,156,4,3);
INSERT INTO Exemplaire VALUES (147,157,4,3);
INSERT INTO Exemplaire VALUES (148,157,4,3);
INSERT INTO Exemplaire VALUES (149,158,4,3);
INSERT INTO Exemplaire VALUES (150,158,4,3);
INSERT INTO Exemplaire VALUES (151,159,4,3);
INSERT INTO Exemplaire VALUES (152,159,4,3);
INSERT INTO Exemplaire VALUES (153,160,4,3);
INSERT INTO Exemplaire VALUES (154,160,4,3);
INSERT INTO Exemplaire VALUES (155,161,4,3);
INSERT INTO Exemplaire VALUES (156,161,4,3);
INSERT INTO Exemplaire VALUES (157,162,4,3);
INSERT INTO Exemplaire VALUES (158,162,4,3);
INSERT INTO Exemplaire VALUES (159,163,4,3);
INSERT INTO Exemplaire VALUES (160,163,4,3);
INSERT INTO Exemplaire VALUES (161,7,4,6);
INSERT INTO Exemplaire VALUES (162,7,4,3);


-----------------------------------------------------
-- PRET
-----------------------------------------------------
INSERT INTO Pret VALUES (161,4,to_number(to_char(CURRENT_DATE, 'j')),  to_number(to_char(CURRENT_DATE + interval '4' day, 'j')));
INSERT INTO Pret VALUES (161,4,to_number(to_char(CURRENT_DATE - interval '14' day, 'j')), to_number(to_char(CURRENT_DATE - interval '10' day, 'j')));
INSERT INTO Pret VALUES (162,4,to_number(to_char(CURRENT_DATE - interval '7' day, 'j')), to_number(to_char(CURRENT_DATE - interval '3' day, 'j')));
INSERT INTO Pret VALUES (162,4,to_number(to_char(CURRENT_DATE - interval '19' day, 'j')), to_number(to_char(CURRENT_DATE - interval '15' day, 'j')));

-----------------------------------------------------
-- RESERVATION
-----------------------------------------------------
INSERT INTO Reservation VALUES (1,162,4,CURRENT_DATE + interval '7' day);
INSERT INTO Reservation VALUES (2,160,4,CURRENT_DATE + interval '14' day);

-----------------------------------------------------
-- LIVRE
-----------------------------------------------------
INSERT INTO Livre VALUES (1, 300, 'ABCD123ASDF98');
INSERT INTO Livre VALUES (2, 400, 'ABCD123ASDF99');
INSERT INTO Livre VALUES (3, 500, 'ABCD123ASDF80');
INSERT INTO Livre VALUES (4, 30, 'ABCD123ASDF20');
INSERT INTO Livre VALUES (5, 40, 'ABCD123ASDF64');
INSERT INTO Livre VALUES (6, 50, 'ABCD123ASDF81');

--autres livres
INSERT INTO Livre VALUES (140, 382,'JFGFDSGFDGGME');
INSERT INTO Livre VALUES (141, 382,'JFDFGSDFGSFME');
INSERT INTO Livre VALUES (142, 382,'JFLDSFGALASME');
INSERT INTO Livre VALUES (143, 382,'JFLSSDFGSFFME');
INSERT INTO Livre VALUES (144, 382,'JFLSODSGDGFME');
INSERT INTO Livre VALUES (145, 382,'JFLSDFSGDFFME');
INSERT INTO Livre VALUES (146, 382,'JFLSOFDBVCXBE');
INSERT INTO Livre VALUES (147, 382,'JFLSOHGFGDFGE');
INSERT INTO Livre VALUES (148, 382,'JFLSODSGMMDFG');
INSERT INTO Livre VALUES (149, 382,'JFLSBVNGSDFME');
INSERT INTO Livre VALUES (150, 382,'JFLDFGPALSFME');
INSERT INTO Livre VALUES (151, 382,'JFLSODSFGDFGE');

-----------------------------------------------------
-- FILM
-----------------------------------------------------
INSERT INTO Film VALUES (28,37);
INSERT INTO Film VALUES (29,44);
INSERT INTO Film VALUES (30,51);
INSERT INTO Film VALUES (31,58);
INSERT INTO Film VALUES (32,65);
INSERT INTO Film VALUES (33,72);
INSERT INTO Film VALUES (34,79);
INSERT INTO Film VALUES (35,86);
INSERT INTO Film VALUES (36,93);
INSERT INTO Film VALUES (37,100);
INSERT INTO Film VALUES (38,107);
INSERT INTO Film VALUES (39,114);
INSERT INTO Film VALUES (40,121);
INSERT INTO Film VALUES (41,128);
INSERT INTO Film VALUES (42,135);
INSERT INTO Film VALUES (43,142);
INSERT INTO Film VALUES (44,149);
INSERT INTO Film VALUES (45,156);
INSERT INTO Film VALUES (46,163);
INSERT INTO Film VALUES (47,170);
INSERT INTO Film VALUES (48,177);
INSERT INTO Film VALUES (49,184);
INSERT INTO Film VALUES (50,191);
INSERT INTO Film VALUES (51,198);
INSERT INTO Film VALUES (52,205);
INSERT INTO Film VALUES (53,212);
INSERT INTO Film VALUES (54,219);
INSERT INTO Film VALUES (55,226);
INSERT INTO Film VALUES (56,233);
INSERT INTO Film VALUES (57,240);
INSERT INTO Film VALUES (58,247);
INSERT INTO Film VALUES (59,254);
INSERT INTO Film VALUES (60,261);
INSERT INTO Film VALUES (61,268);
INSERT INTO Film VALUES (62,275);
INSERT INTO Film VALUES (63,282);
INSERT INTO Film VALUES (64,289);
INSERT INTO Film VALUES (65,296);
INSERT INTO Film VALUES (66,303);
INSERT INTO Film VALUES (67,310);
INSERT INTO Film VALUES (68,317);
INSERT INTO Film VALUES (69,324);
INSERT INTO Film VALUES (70,331);
INSERT INTO Film VALUES (71,338);
INSERT INTO Film VALUES (72,345);
INSERT INTO Film VALUES (73,352);
INSERT INTO Film VALUES (74,359);
INSERT INTO Film VALUES (75,366);
INSERT INTO Film VALUES (76,373);
INSERT INTO Film VALUES (77,380);
INSERT INTO Film VALUES (78,387);
INSERT INTO Film VALUES (79,394);
INSERT INTO Film VALUES (80,401);
INSERT INTO Film VALUES (81,408);
INSERT INTO Film VALUES (82,415);
INSERT INTO Film VALUES (83,422);
INSERT INTO Film VALUES (84,429);
INSERT INTO Film VALUES (85,436);
INSERT INTO Film VALUES (86,443);
INSERT INTO Film VALUES (87,450);
INSERT INTO Film VALUES (88,457);
INSERT INTO Film VALUES (89,464);
INSERT INTO Film VALUES (90,471);
INSERT INTO Film VALUES (91,478);
INSERT INTO Film VALUES (92,485);
INSERT INTO Film VALUES (93,492);
INSERT INTO Film VALUES (94,499);
INSERT INTO Film VALUES (95,506);
INSERT INTO Film VALUES (96,513);
INSERT INTO Film VALUES (97,520);
INSERT INTO Film VALUES (98,527);
INSERT INTO Film VALUES (99,534);
INSERT INTO Film VALUES (100,541);
INSERT INTO Film VALUES (101,548);
INSERT INTO Film VALUES (102,555);
INSERT INTO Film VALUES (103,562);
INSERT INTO Film VALUES (104,569);
INSERT INTO Film VALUES (105,576);
INSERT INTO Film VALUES (106,583);
INSERT INTO Film VALUES (107,590);
INSERT INTO Film VALUES (108,597);
INSERT INTO Film VALUES (109,604);
INSERT INTO Film VALUES (110,611);
INSERT INTO Film VALUES (111,618);
INSERT INTO Film VALUES (112,625);
INSERT INTO Film VALUES (113,632);
INSERT INTO Film VALUES (114,639);
INSERT INTO Film VALUES (115,646);
INSERT INTO Film VALUES (116,653);
INSERT INTO Film VALUES (117,660);
INSERT INTO Film VALUES (118,667);
INSERT INTO Film VALUES (119,674);
INSERT INTO Film VALUES (120,681);
INSERT INTO Film VALUES (121,688);
INSERT INTO Film VALUES (122,695);
INSERT INTO Film VALUES (123,702);
INSERT INTO Film VALUES (124,709);
INSERT INTO Film VALUES (125,716);
INSERT INTO Film VALUES (126,723);
INSERT INTO Film VALUES (127,730);
INSERT INTO Film VALUES (128,737);
INSERT INTO Film VALUES (129,744);
INSERT INTO Film VALUES (130,751);
INSERT INTO Film VALUES (131,758);
INSERT INTO Film VALUES (132,765);
INSERT INTO Film VALUES (133,772);
INSERT INTO Film VALUES (134,779);
INSERT INTO Film VALUES (135,786);
INSERT INTO Film VALUES (136,793);
INSERT INTO Film VALUES (137,800);
INSERT INTO Film VALUES (138,807);
INSERT INTO Film VALUES (139,814);

-----------------------------------------------------
-- MUSIQUE
-----------------------------------------------------
INSERT INTO Musique VALUES (7,120);
INSERT INTO Musique VALUES (8,120);
INSERT INTO Musique VALUES (9,120);
INSERT INTO Musique VALUES (10,90);
INSERT INTO Musique VALUES (11,90);
INSERT INTO Musique VALUES (12,90);
INSERT INTO Musique VALUES (13,60);
INSERT INTO Musique VALUES (14,60);
INSERT INTO Musique VALUES (15,60);
INSERT INTO Musique VALUES (16,30);
INSERT INTO Musique VALUES (17,30);
INSERT INTO Musique VALUES (18,30);
INSERT INTO Musique VALUES (19,30);
INSERT INTO Musique VALUES (20,30);
INSERT INTO Musique VALUES (21,30);
INSERT INTO Musique VALUES (22,30);
INSERT INTO Musique VALUES (23,30);
INSERT INTO Musique VALUES (24,30);
INSERT INTO Musique VALUES (25,30);
INSERT INTO Musique VALUES (26,30);
INSERT INTO Musique VALUES (27,30);

-- autres musique
INSERT INTO Musique VALUES (152,63);
INSERT INTO Musique VALUES (153,63);
INSERT INTO Musique VALUES (154,63);
INSERT INTO Musique VALUES (155,63);
INSERT INTO Musique VALUES (156,63);
INSERT INTO Musique VALUES (157,63);
INSERT INTO Musique VALUES (158,63);
INSERT INTO Musique VALUES (159,63);
INSERT INTO Musique VALUES (160,63);
INSERT INTO Musique VALUES (161,63);
INSERT INTO Musique VALUES (162,63);
INSERT INTO Musique VALUES (163,63);

-----------------------------------------------------
-- ACTEUR
-----------------------------------------------------

-----------------------------------------------------
-- INTERPRETE
-----------------------------------------------------

-----------------------------------------------------
-- COMPOSITEUR
-----------------------------------------------------
INSERT INTO Compositeur VALUES('P008');
INSERT INTO Compositeur VALUES('P009');
INSERT INTO Compositeur VALUES('P010');

-----------------------------------------------------
-- FILMACTEUR
-----------------------------------------------------

-----------------------------------------------------
-- MUSIQUEINTERPRETE
-----------------------------------------------------

-----------------------------------------------------
-- MUSIQUECOMPOSITEUR
-----------------------------------------------------
-- Autre musique
INSERT INTO MusiqueCompositeur VALUES (152,'P008');
INSERT INTO MusiqueCompositeur VALUES (153,'P008');
INSERT INTO MusiqueCompositeur VALUES (154,'P008');
INSERT INTO MusiqueCompositeur VALUES (155,'P009');
INSERT INTO MusiqueCompositeur VALUES (156,'P009');
INSERT INTO MusiqueCompositeur VALUES (157,'P009');
INSERT INTO MusiqueCompositeur VALUES (158,'P010');
INSERT INTO MusiqueCompositeur VALUES (159,'P010');
INSERT INTO MusiqueCompositeur VALUES (160,'P010');
INSERT INTO MusiqueCompositeur VALUES (161,'P010');
INSERT INTO MusiqueCompositeur VALUES (162,'P010');
INSERT INTO MusiqueCompositeur VALUES (163,'P010');

-----------------------------------------------------
-- OEUVREAUTEUR
-----------------------------------------------------
-- Autres livres
INSERT INTO OeuvreAuteur VALUES (140,'P006');
INSERT INTO OeuvreAuteur VALUES (141,'P006');
INSERT INTO OeuvreAuteur VALUES (142,'P006');
INSERT INTO OeuvreAuteur VALUES (143,'P006');
INSERT INTO OeuvreAuteur VALUES (144,'P006');
INSERT INTO OeuvreAuteur VALUES (145,'P006');
INSERT INTO OeuvreAuteur VALUES (146,'P007');
INSERT INTO OeuvreAuteur VALUES (147,'P007');
INSERT INTO OeuvreAuteur VALUES (148,'P007');
INSERT INTO OeuvreAuteur VALUES (149,'P007');
INSERT INTO OeuvreAuteur VALUES (150,'P007');
INSERT INTO OeuvreAuteur VALUES (151,'P007');


-----------------------------------------------------
-- EMPLACEMENT
-----------------------------------------------------
INSERT INTO Emplacement VALUES (1,1,1,1);
INSERT INTO Emplacement VALUES (1,1,2,2);
INSERT INTO Emplacement VALUES (1,1,3,3);
INSERT INTO Emplacement VALUES (1,1,4,4);
INSERT INTO Emplacement VALUES (1,1,5,5);
INSERT INTO Emplacement VALUES (1,1,6,6);
INSERT INTO Emplacement VALUES (1,1,7,7);
INSERT INTO Emplacement VALUES (1,1,8,8);
INSERT INTO Emplacement VALUES (1,1,9,9);
INSERT INTO Emplacement VALUES (1,1,10,10);
INSERT INTO Emplacement VALUES (2,1,1,11);
INSERT INTO Emplacement VALUES (2,1,2,12);
INSERT INTO Emplacement VALUES (2,1,3,13);
INSERT INTO Emplacement VALUES (2,1,4,14);
INSERT INTO Emplacement VALUES (2,1,5,15);
INSERT INTO Emplacement VALUES (2,1,6,16);
INSERT INTO Emplacement VALUES (2,1,7,17);
INSERT INTO Emplacement VALUES (2,1,8,18);
INSERT INTO Emplacement VALUES (2,1,9,19);
INSERT INTO Emplacement VALUES (2,1,10,20);
INSERT INTO Emplacement VALUES (3,1,1,21);
INSERT INTO Emplacement VALUES (3,1,2,22);
INSERT INTO Emplacement VALUES (3,1,3,23);
INSERT INTO Emplacement VALUES (3,1,4,24);
INSERT INTO Emplacement VALUES (3,1,5,25);
INSERT INTO Emplacement VALUES (3,1,6,26);
INSERT INTO Emplacement VALUES (3,1,7,27);
INSERT INTO Emplacement VALUES (3,1,8,28);
INSERT INTO Emplacement VALUES (3,1,9,29);
INSERT INTO Emplacement VALUES (3,1,10,30);
INSERT INTO Emplacement VALUES (4,1,1,31);
INSERT INTO Emplacement VALUES (4,1,2,32);
INSERT INTO Emplacement VALUES (4,1,3,33);
INSERT INTO Emplacement VALUES (4,1,4,34);
INSERT INTO Emplacement VALUES (4,1,5,35);
INSERT INTO Emplacement VALUES (4,1,6,36);
INSERT INTO Emplacement VALUES (4,1,7,37);
INSERT INTO Emplacement VALUES (4,1,8,38);
INSERT INTO Emplacement VALUES (4,1,9,39);
INSERT INTO Emplacement VALUES (4,1,10,40);
INSERT INTO Emplacement VALUES (5,1,1,41);
INSERT INTO Emplacement VALUES (5,1,2,42);
INSERT INTO Emplacement VALUES (5,1,3,43);
INSERT INTO Emplacement VALUES (5,1,4,44);
INSERT INTO Emplacement VALUES (5,1,5,45);
INSERT INTO Emplacement VALUES (5,1,6,46);
INSERT INTO Emplacement VALUES (5,1,7,47);
INSERT INTO Emplacement VALUES (5,1,8,48);
INSERT INTO Emplacement VALUES (5,1,9,49);
INSERT INTO Emplacement VALUES (5,1,10,50);
INSERT INTO Emplacement VALUES (1,2,1,51);
INSERT INTO Emplacement VALUES (1,2,2,52);
INSERT INTO Emplacement VALUES (1,2,3,53);
INSERT INTO Emplacement VALUES (1,2,4,54);
INSERT INTO Emplacement VALUES (1,2,5,55);
INSERT INTO Emplacement VALUES (1,2,6,56);
INSERT INTO Emplacement VALUES (1,2,7,57);
INSERT INTO Emplacement VALUES (1,2,8,58);
INSERT INTO Emplacement VALUES (1,2,9,59);
INSERT INTO Emplacement VALUES (1,2,10,60);
INSERT INTO Emplacement VALUES (2,2,1,61);
INSERT INTO Emplacement VALUES (2,2,2,62);
INSERT INTO Emplacement VALUES (2,2,3,63);
INSERT INTO Emplacement VALUES (2,2,4,64);
INSERT INTO Emplacement VALUES (2,2,5,65);
INSERT INTO Emplacement VALUES (2,2,6,66);
INSERT INTO Emplacement VALUES (2,2,7,67);
INSERT INTO Emplacement VALUES (2,2,8,68);
INSERT INTO Emplacement VALUES (2,2,9,69);
INSERT INTO Emplacement VALUES (2,2,10,70);
INSERT INTO Emplacement VALUES (3,2,1,71);
INSERT INTO Emplacement VALUES (3,2,2,72);
INSERT INTO Emplacement VALUES (3,2,3,73);
INSERT INTO Emplacement VALUES (3,2,4,74);
INSERT INTO Emplacement VALUES (3,2,5,75);
INSERT INTO Emplacement VALUES (3,2,6,76);
INSERT INTO Emplacement VALUES (3,2,7,77);
INSERT INTO Emplacement VALUES (3,2,8,78);
INSERT INTO Emplacement VALUES (3,2,9,79);
INSERT INTO Emplacement VALUES (3,2,10,80);
INSERT INTO Emplacement VALUES (4,2,1,81);
INSERT INTO Emplacement VALUES (4,2,2,82);
INSERT INTO Emplacement VALUES (4,2,3,83);
INSERT INTO Emplacement VALUES (4,2,4,84);
INSERT INTO Emplacement VALUES (4,2,5,85);
INSERT INTO Emplacement VALUES (4,2,6,86);
INSERT INTO Emplacement VALUES (4,2,7,87);
INSERT INTO Emplacement VALUES (4,2,8,88);
INSERT INTO Emplacement VALUES (4,2,9,89);
INSERT INTO Emplacement VALUES (4,2,10,90);
INSERT INTO Emplacement VALUES (5,2,1,91);
INSERT INTO Emplacement VALUES (5,2,2,92);
INSERT INTO Emplacement VALUES (5,2,3,93);
INSERT INTO Emplacement VALUES (5,2,4,94);
INSERT INTO Emplacement VALUES (5,2,5,95);
INSERT INTO Emplacement VALUES (5,2,6,96);
INSERT INTO Emplacement VALUES (5,2,7,97);
INSERT INTO Emplacement VALUES (5,2,8,98);
INSERT INTO Emplacement VALUES (5,2,9,99);
INSERT INTO Emplacement VALUES (5,2,10,100);
INSERT INTO Emplacement VALUES (1,3,1,101);
INSERT INTO Emplacement VALUES (1,3,2,102);
INSERT INTO Emplacement VALUES (1,3,3,103);
INSERT INTO Emplacement VALUES (1,3,4,104);
INSERT INTO Emplacement VALUES (1,3,5,105);
INSERT INTO Emplacement VALUES (1,3,6,106);
INSERT INTO Emplacement VALUES (1,3,7,107);
INSERT INTO Emplacement VALUES (1,3,8,108);
INSERT INTO Emplacement VALUES (1,3,9,109);
INSERT INTO Emplacement VALUES (1,3,10,110);
INSERT INTO Emplacement VALUES (2,3,1,111);
INSERT INTO Emplacement VALUES (2,3,2,112);
INSERT INTO Emplacement VALUES (2,3,3,113);
INSERT INTO Emplacement VALUES (2,3,4,114);
INSERT INTO Emplacement VALUES (2,3,5,115);
INSERT INTO Emplacement VALUES (2,3,6,116);
INSERT INTO Emplacement VALUES (2,3,7,117);
INSERT INTO Emplacement VALUES (2,3,8,118);
INSERT INTO Emplacement VALUES (2,3,9,119);
INSERT INTO Emplacement VALUES (2,3,10,120);
INSERT INTO Emplacement VALUES (3,3,1,121);
INSERT INTO Emplacement VALUES (3,3,2,122);
INSERT INTO Emplacement VALUES (3,3,3,123);
INSERT INTO Emplacement VALUES (3,3,4,124);
INSERT INTO Emplacement VALUES (3,3,5,125);
INSERT INTO Emplacement VALUES (3,3,6,126);
INSERT INTO Emplacement VALUES (3,3,7,127);
INSERT INTO Emplacement VALUES (3,3,8,128);
INSERT INTO Emplacement VALUES (3,3,9,129);
INSERT INTO Emplacement VALUES (3,3,10,130);
INSERT INTO Emplacement VALUES (4,3,1,131);
INSERT INTO Emplacement VALUES (4,3,2,132);
INSERT INTO Emplacement VALUES (4,3,3,133);
INSERT INTO Emplacement VALUES (4,3,4,134);
INSERT INTO Emplacement VALUES (4,3,5,135);
INSERT INTO Emplacement VALUES (4,3,6,136);
INSERT INTO Emplacement VALUES (4,3,7,137);
INSERT INTO Emplacement VALUES (4,3,8,138);
INSERT INTO Emplacement VALUES (4,3,9,139);
INSERT INTO Emplacement VALUES (4,3,10,140);
INSERT INTO Emplacement VALUES (5,3,1,141);
INSERT INTO Emplacement VALUES (5,3,2,142);
INSERT INTO Emplacement VALUES (5,3,3,143);
INSERT INTO Emplacement VALUES (5,3,4,144);
INSERT INTO Emplacement VALUES (5,3,5,145);
INSERT INTO Emplacement VALUES (5,3,6,146);
INSERT INTO Emplacement VALUES (5,3,7,147);
INSERT INTO Emplacement VALUES (5,3,8,148);
INSERT INTO Emplacement VALUES (5,3,9,149);
INSERT INTO Emplacement VALUES (5,3,10,150);
INSERT INTO Emplacement VALUES (1,4,1,151);
INSERT INTO Emplacement VALUES (1,4,2,152);
INSERT INTO Emplacement VALUES (1,4,3,153);
INSERT INTO Emplacement VALUES (1,4,4,154);
INSERT INTO Emplacement VALUES (1,4,5,155);
INSERT INTO Emplacement VALUES (1,4,6,156);
INSERT INTO Emplacement VALUES (1,4,7,157);
INSERT INTO Emplacement VALUES (1,4,8,158);
INSERT INTO Emplacement VALUES (1,4,9,159);
INSERT INTO Emplacement VALUES (1,4,10,160);
INSERT INTO Emplacement VALUES (2,4,1,NULL);
INSERT INTO Emplacement VALUES (2,4,2,NULL);
INSERT INTO Emplacement VALUES (2,4,3,NULL);

SET ECHO OFF
SET PAGESIZE 30
