--
-- Script de creation des triggers
-- Auteurs
-- Code permanent: Steve Boucher    (BOUS30097608)
-- Code permanent: Maxime Girard    (GIRM30058500)
-- Code permanent: Mathieu Tanguay  (TANM28028902)
-- Code permanent: Dominic Turgeon  (TURD14058501)
--

--parametre de sessions
--cette partie la n'estait pas dans le template du prof, c'est moi qui la ajoute
--on pourra le dropper a la fin du tp
--Steve B
--SET SERVEROUTPUT ON
--ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'

SET ECHO ON


-- Block un pret si l'oeuvre est en consultation seulement
CREATE OR REPLACE TRIGGER blkPretConsultationSeulement
BEFORE INSERT OR UPDATE ON Pret
FOR EACH ROW
DECLARE
	cnt number;
BEGIN
	select count(*) into cnt from exemplaire e, oeuvre o
	where e.oeuvreNumero = o.numero and
			  consultationSeulement = 'V' and
			  e.numero = :NEW.exemplaireNumero;


	IF cnt > 0 THEN
			RAISE_APPLICATION_ERROR(-20000, 'L''oeuvre est en consultation seulement.');
	END IF;
END;
/

-- Block une reservation si l'oeuvre est en consultation seulement
CREATE OR REPLACE TRIGGER blkReservConsultationSeulement
BEFORE INSERT OR UPDATE ON Reservation
FOR EACH ROW
DECLARE
	cnt number;
BEGIN
	select count(*) into cnt from exemplaire e, oeuvre o
	where e.oeuvreNumero = o.numero and
			  consultationSeulement = 'V' and
			  e.numero = :NEW.exemplaireNumero;


	IF cnt > 0 THEN
			RAISE_APPLICATION_ERROR(-20000, 'L''oeuvre est en consultation seulement.');
	END IF;
END;
/
-- Bloque un pret si l'exemplaire est en mauvais état
create or replace TRIGGER blkPretMauvaisEtat
BEFORE INSERT OR UPDATE ON Pret
FOR EACH ROW
DECLARE
	cnt number;
BEGIN
	select count(*) into cnt from exemplaire e
  where etatExemplaireNumero IN (6,7) and
			  e.numero = :NEW.exemplaireNumero;

	IF cnt > 0 THEN
			RAISE_APPLICATION_ERROR(-20000, 'L''oeuvre est en mauvais état.');
	END IF;
END;
/
-- Bloque une reservation si l'exemplaire est en mauvais état
create or replace TRIGGER blkReservMauvaisEtat
BEFORE INSERT OR UPDATE ON Reservation
FOR EACH ROW
DECLARE
	cnt number;
BEGIN
	select count(*) into cnt from exemplaire e
  where etatExemplaireNumero IN (6,7) and
			  e.numero = :NEW.exemplaireNumero;

	IF cnt > 0 THEN
			RAISE_APPLICATION_ERROR(-20000, 'L''oeuvre est en mauvais état.');
	END IF;
END;
/

-- Bloque une pret si l'adherent a deja 3 prets ouverts
create or replace TRIGGER blkPlusQueTroisExemplaires
BEFORE INSERT OR UPDATE ON Pret
FOR EACH ROW
DECLARE
	cnt number;
BEGIN
	select count(*) into cnt from Pret p
	where p.dateRetour IS NULL
	AND p.adherentNumero = :NEW.adherentNumero;

	IF cnt >= 3 THEN
			RAISE_APPLICATION_ERROR(-20001, 'Trop de prêts');
	END IF;
END;
/

SET ECHO OFF
