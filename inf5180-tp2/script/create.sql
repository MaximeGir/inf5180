--
-- Script de creation des tables
-- Auteurs
-- Code permanent: Steve Boucher    (BOUS30097608)
-- Code permanent: Maxime Girard    (GIRM30058500)
-- Code permanent: Mathieu Tanguay  (TANM28028902)
-- Code permanent: Dominic Turgeon  (TURD14058501)
-- 
--
SET ECHO ON

--parametre de sessions
--cette partie la n'etait pas dans le template du prof, c'est moi qui la ajoute
--on pourra le dropper a la fin du tp
--Steve B
SET SERVEROUTPUT ON
ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'
/

 
--TODO

--Valeur par default
--check constraints dans table Pret


CREATE TABLE Categorie(
	numero					INTEGER			NOT NULL,
	typeCategorie			VARCHAR(20) 	NOT NULL,
	nomCategorie			VARCHAR(30)		NOT NULL,
	CONSTRAINT categorie_pk 				PRIMARY KEY (numero)
)
/

CREATE TABLE Support(
	numero					INTEGER			NOT NULL,
	nature					VARCHAR(20)		NOT NULL,
	CONSTRAINT support_pk 					PRIMARY KEY (numero)
)
/

CREATE TABLE Etat_exemplaire(
	numero					INTEGER			NOT NULL,
	etatExemplaire 			VARCHAR(30)		NOT NULL,
	CONSTRAINT etat_exemplaire_pk 			PRIMARY KEY (numero)
)
/

CREATE TABLE Personne(
	identifiant 			VARCHAR(20)		NOT NULL,
	nom 					VARCHAR(255)	NOT NULL,
	prenom					VARCHAR(255)	NOT NULL,
	CONSTRAINT personne_pk PRIMARY KEY (identifiant)
)
/

 
CREATE TABLE Adherent(
	numero 					INTEGER			NOT NULL,
	personneIdentifiant		VARCHAR(20)		NOT NULL,
	adresse 				VARCHAR(100)	NOT NULL,
	telephone 				VARCHAR(10)		NOT NULL,
	etat 					VARCHAR(100)	NOT NULL,
	CONSTRAINT adherent_pk 					PRIMARY KEY (numero),
	CONSTRAINT adherent_fk 					FOREIGN KEY (personneIdentifiant) 	REFERENCES Personne (identifiant)
)
/

CREATE TABLE Oeuvre(
	numero					INTEGER 		NOT NULL,
	categorieNumero			INTEGER,
	titre					VARCHAR(100) 	NOT NULL,
	consultationSeulement	VARCHAR(1) 		NOT NULL,
	CONSTRAINT oeuvre_pk 					PRIMARY KEY (numero),
	CONSTRAINT oeuvre_fk 					FOREIGN KEY (categorieNumero)		REFERENCES Categorie (numero),
	CONSTRAINT oeuvre_chk 					CHECK(consultationSeulement = 'V' OR consultationSeulement = 'F')
)
/

CREATE TABLE Auteur(
	identifiant 			VARCHAR(20) 		NOT NULL,
	CONSTRAINT personne_oeuvre_pk 			PRIMARY KEY (identifiant),
	CONSTRAINT personne_oeuvre_fk2 			FOREIGN KEY (identifiant)			REFERENCES Personne (identifiant)
)
/

CREATE TABLE Exemplaire(
	numero					INTEGER 		NOT NULL,
	oeuvreNumero			INTEGER		 	NOT NULL,
	supportNumero			INTEGER 		NOT NULL,
	etatExemplaireNumero	INTEGER			NOT NULL,
	CONSTRAINT exemplaire_pk 				PRIMARY KEY (numero),
	CONSTRAINT exemplaire_fk 				FOREIGN KEY (oeuvreNumero)        	REFERENCES Oeuvre (numero),
	CONSTRAINT exemplaire_fk2				FOREIGN KEY (etatExemplaireNumero) 	REFERENCES Etat_exemplaire (numero),
	CONSTRAINT exemplaire_fk3 				FOREIGN KEY (supportNumero)        	REFERENCES Support (numero)
)
/

CREATE TABLE Pret(
	exemplaireNumero		INTEGER         NOT NULL,
	adherentNumero    		INTEGER     	NOT NULL,
	dateEmprunt	   			INTEGER     	NOT NULL,
	dateRetour	      		INTEGER,
	CONSTRAINT pret_pk 						PRIMARY KEY (exemplaireNumero, adherentNumero, dateEmprunt),
	CONSTRAINT pret_fk 						FOREIGN KEY (exemplaireNumero) 		REFERENCES Exemplaire(numero),
	CONSTRAINT pret_fk2 					FOREIGN KEY (adherentNumero) 		REFERENCES Adherent(numero)
 --CHECK - doit dateRetour entre 0 et 4 jours de dateEmprunt
)
/


CREATE TABLE Reservation(
	noReservation			INTEGER			NOT NULL,
	exemplaireNumero 		INTEGER			NOT NULL,
	adherentNumero 			INTEGER			NOT NULL,
	dateReservation			TIMESTAMP		NOT NULL,
	CONSTRAINT reservation_pk 				PRIMARY KEY (exemplaireNumero, adherentNumero, dateReservation),
	CONSTRAINT reservation_fk 				FOREIGN KEY (exemplaireNumero)		REFERENCES Exemplaire(numero),
	CONSTRAINT reservation_fk2 				FOREIGN KEY (adherentNumero)   		REFERENCES Adherent(numero)
)
/

CREATE TABLE Livre(
	numero					INTEGER 		NOT NULL,
	nbPages					INTEGER 		NOT NULL,
	ISBN					VARCHAR(20)		NOT NULL,
	CONSTRAINT livre_pk 					PRIMARY KEY (numero),
	CONSTRAINT livre_fk 					FOREIGN KEY (numero) REFERENCES Oeuvre,
	CONSTRAINT livre_chk					CHECK(nbPages > 0),
	CONSTRAINT livre_un 					UNIQUE(ISBN)
)
/

CREATE TABLE Film(
	numero					INTEGER			NOT NULL,
	duree					INTEGER			NOT NULL,
	CONSTRAINT film_pk 						PRIMARY KEY (numero),
	CONSTRAINT film_fk 						FOREIGN KEY (numero)              	REFERENCES Oeuvre(numero)
)
/

CREATE TABLE Musique(
	numero					INTEGER 		NOT NULL,
	personneIdentifiant		VARCHAR(20) 	NOT NULL,
	CONSTRAINT musique_pk 					PRIMARY KEY (numero),
	CONSTRAINT musique_fk 					FOREIGN KEY (numero)				REFERENCES Oeuvre(numero)
)
/

CREATE TABLE Acteur (
	identifiant 			VARCHAR(10) 	NOT NULL,
	CONSTRAINT acteur_pk 					PRIMARY KEY (identifiant),
	CONSTRAINT acteur_fk 					FOREIGN KEY (identifiant) 			REFERENCES Personne(identifiant)
)
/

CREATE TABLE Interprete (
	identifiant 			VARCHAR(10) 	NOT NULL,
	CONSTRAINT interprete_pk 				PRIMARY KEY (identifiant),
	CONSTRAINT interprete_fk 				FOREIGN KEY (identifiant) 			REFERENCES Personne(identifiant)
)
/

CREATE TABLE Compositeur (
	identifiant 			VARCHAR(10)		NOT NULL,
	CONSTRAINT compositeur_pk 				PRIMARY KEY (identifiant),
	CONSTRAINT compositeur_fk 				FOREIGN KEY (identifiant) 			REFERENCES Personne(identifiant)
)
/

CREATE TABLE FilmActeur(
	filmNumero 				INTEGER			NOT NULL,
	acteurIdentifiant		VARCHAR(20)		NOT NULL,
	CONSTRAINT filmNumero_fk 				FOREIGN KEY (filmNumero) 			REFERENCES Film(numero),
	CONSTRAINT acteurIdentifiant_fk 		FOREIGN KEY (acteurIdentifiant) 	REFERENCES Acteur(identifiant)
)
/

CREATE TABLE MusiqueInterprete(
	musiqueNumero 			INTEGER			NOT NULL,
	interpreteIdentifiant 	VARCHAR(20)		NOT NULL,
	CONSTRAINT mi_musiqueNumero_fk 			FOREIGN KEY (musiqueNumero) 		REFERENCES Musique(numero),
	CONSTRAINT mi_interpreteIdentifiant_fk 	FOREIGN KEY (interpreteIdentifiant) REFERENCES Interprete(identifiant)
)
/

CREATE TABLE MusiqueCompositeur(
	musiqueNumero 			INTEGER			NOT NULL,
	interpreteIdentifiant 	VARCHAR(20)		NOT NULL,
	CONSTRAINT mc_musiqueNumero_fk 			FOREIGN KEY (musiqueNumero) 		REFERENCES Musique(numero),
	CONSTRAINT mc_interpreteIdentifiant_fk 	FOREIGN KEY (interpreteIdentifiant) REFERENCES Compositeur (identifiant)
)
/

CREATE TABLE OeuvreAuteur(
	oeuvreNumero 			INTEGER			NOT NULL,
	auteurIdentifiant 		VARCHAR(20)		NOT NULL,
	CONSTRAINT oa_oeuvreNumero_fk 			FOREIGN KEY (oeuvreNumero) 			REFERENCES Oeuvre(numero),
	CONSTRAINT oa_auteurIdentifaint 		FOREIGN KEY (auteurIdentifiant) 	REFERENCES Auteur(identifiant)
)
/

CREATE TABLE Emplacement(
	travee					INTEGER         NOT NULL,
	etagere					INTEGER     	NOT NULL,
	rayon					INTEGER     	NOT NULL,
	exemplaireNumero		INTEGER,
	CONSTRAINT emplacement_pk 				PRIMARY KEY (travee, etagere, rayon),
	CONSTRAINT emplacement_fk2 				FOREIGN KEY (exemplaireNumero) 		REFERENCES Exemplaire
)
/

SET ECHO OFF
