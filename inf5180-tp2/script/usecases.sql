-- Ceci correspond a la question 2.4 de l'enonce

-----------------------------------------------------
-- USE CASE - RESERVATION
-----------------------------------------------------

-- A)
-- Requête pour identifier les oeuvres qui correspondent
-- au titre rechercher et retourne les exemplaires qui ne sont pas
-- en mauvais etat et qui n'ont pas de pret ou de reservation pour
-- aujourd'hui

SELECT Exemplaire.numero FROM Oeuvre
INNER JOIN Exemplaire ON Oeuvre.numero = Exemplaire.oeuvreNumero
WHERE titre = ('Star Wars I')
AND ROWNUM <= 1
AND etatExemplaireNumero NOT IN (6,7)
AND (
    SELECT COUNT(Pret.exemplaireNumero) FROM Pret WHERE Pret.exemplaireNumero = Exemplaire.numero 
    AND (
      (Pret.dateRetour IS NULL)
      OR
      (CURRENT_DATE BETWEEN Pret.dateEmprunt AND Pret.dateRetour)
    )
) = 0
AND (
  SELECT COUNT(Reservation.exemplaireNumero) FROM Reservation WHERE Reservation.exemplaireNumero = Exemplaire.numero
  AND (CURRENT_DATE BETWEEN Reservation.dateReservation AND Reservation.dateReservation + interval '4' day)
) = 0;

-- B)
-- On identifie le numero de l'adherent selon ses informations de bases

SELECT numero FROM Adherent
WHERE personneIdentifiant =
  (SELECT identifiant FROM Personne
  WHERE nom = 'Turgeon'
  AND prenom = 'Dominic')
AND adresse = '4 rue universite'
AND telephone =  '555-3333';

-- C)
-- On utilise les informations recueillies avec les deux requêtes précédentes et
-- on fait une insertion dans la table des reservations

-- 162 provient du resultat de la requete en A)
-- 4 provient du resultat de la requete en B)

INSERT INTO Reservation VALUES (1,162,4,CURRENT_DATE);

-----------------------------------------------------
-- USE CASE - PRET
-----------------------------------------------------

-- A)
-- Lorsque l'adherent se presente au comptoir avec un exemplaire dans les mains (dans ce cas-ci, l'exemplaire #162)
-- pour faire un pret, on verifie s'il n'y pas de reservation pour cet exemplaire qui rentre en conflit avec la demande de pret

SELECT numero FROM Exemplaire
WHERE numero = 162
AND ROWNUM <= 1
AND Exemplaire.etatExemplaireNumero NOT IN (6,7)
AND (
  SELECT COUNT(Reservation.exemplaireNumero) FROM Reservation WHERE Reservation.exemplaireNumero = Exemplaire.numero
  AND (CURRENT_DATE BETWEEN Reservation.dateReservation AND Reservation.dateReservation + interval '4' day)
) = 0;

-- B)
-- On identifie le numero de l'adherent selon ses informations de bases

SELECT numero FROM Adherent
WHERE personneIdentifiant =
  (SELECT identifiant FROM Personne
  WHERE nom = 'Turgeon'
  AND prenom = 'Dominic')
AND adresse = '4 rue universite'
AND telephone =  '555-3333';

-- C)
-- On utilise les informations recueillies avec les deux requêtes précédentes et
-- on fait une insertion dans la table des reservations

-- 162 provient du resultat de la requete en A)
-- 4 provient du resultat de la requete en B)
-- NULL signifie que le pret est ouvert.

INSERT INTO Pret VALUES (162,4,CURRENT_DATE,NULL);

-----------------------------------------------------
-- USE CASE - RETOURS
-----------------------------------------------------

-- A)
-- Cette requete va placer la date de retour dans la colonne correspondante dans
-- le pret ouvert pour l'exemplaire voulu (exemplaire #162 dans ce cas-ci). On
-- s'assure que la date de retour s'incrit dans un pret ouvert seulement.

UPDATE Pret 
SET dateRetour = CURRENT_DATE
WHERE Pret.numeroExemplaire = 162
AND Pret.dateRetour = NULL