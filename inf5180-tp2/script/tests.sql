--
-- Script de test des triggers
-- Auteurs
-- Code permanent: Steve Boucher    (BOUS30097608)
-- Code permanent: Maxime Girard    (GIRM30058500)
-- Code permanent: Mathieu Tanguay  (TANM28028902)
-- Code permanent: Dominic Turgeon  (TURD14058501)
--
SET LINESIZE 160
SET ECHO ON

-- test trigger blkPretConsultationSeulement (Block un pret si l'oeuvre est en consultation seulement)
INSERT INTO Pret VALUES (1,4,to_number(to_char(CURRENT_DATE, 'j')),to_number(to_char(CURRENT_DATE, 'j')));

-- test trigger blkReservConsultationSeulement (Block une réservation si l'oeuvre est en consultation seulement)
INSERT INTO Reservation VALUES (1,1,4,CURRENT_DATE);

-- test trigger blkPretMauvaisEtat (Block un pret si l'oeuvre est en mauvais état)
INSERT INTO Pret VALUES (113,4,to_number(to_char(CURRENT_DATE, 'j')),to_number(to_char(CURRENT_DATE, 'j')));

-- test trigger blkReservMauvaisEtat (Block une réservation si l'oeuvre est en mauvais état)
INSERT INTO Reservation VALUES (1,114,4,CURRENT_DATE);

-- test trigger blkPlusQueTroisExemplaires (Bloque une pret si l'adherent a deja 3 prets ouverts)
INSERT INTO Pret VALUES (150, 4, to_number(to_char(CURRENT_DATE, 'j')), NULL);
INSERT INTO Pret VALUES (151, 4, to_number(to_char(CURRENT_DATE, 'j')), NULL);
INSERT INTO Pret VALUES (152, 4, to_number(to_char(CURRENT_DATE, 'j')), NULL);
INSERT INTO Pret VALUES (153, 4, to_number(to_char(CURRENT_DATE, 'j')), NULL);

SET ECHO OFF
SET PAGESIZE 30
