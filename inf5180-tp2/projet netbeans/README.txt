-------------------------------------------
AUTEURS
-------------------------------------------

CROCRODILE TEAM:
Steve Boucher - BOUS30097608 - steve.boucher@live.ca
Maxime Girard - GIRM30058500 - girard.maxime.3@courrier.uqam.ca
Mathieu Tanguay
Dominic Turgeon - TURD14058501 - turgeon.dominic@courrier.uqam.ca

Pour Zaier Zied
INF5180-10 Travail Pratique II

--------------------------------------------

NOTES IMPORTANTES
~~~~~~~~~~~~~~~~~

Le programme prendra de 10 � 20 seconde pour trouver une connection stable.
Le programme prendra ensuite de 10 � 30 secondes pour afficher l'interface graphique (Chargement des tables).
Donc patience s'il vous pla�t!

Utilisez ce programme en changeant le fichier hibernate.cfg.xml :

    <property name="hibernate.connection.username">votreCodeMS</property>
    <property name="hibernate.connection.password">votrePassword</property>

Remplacer ces lignes par les valeurs appropri�s et compilez.

---------------------------------------------

Musique �cout� pendant la programmation : 

https://www.youtube.com/watch?v=Od1ZbV9rutI

