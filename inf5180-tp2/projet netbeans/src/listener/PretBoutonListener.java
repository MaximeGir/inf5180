package listener;

import gui.MainWindow;
import gui.PretPopup;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import mapping.Adherent;
import mapping.Exemplaire;
import mapping.Oeuvre;
import mapping.Pret;
import mapping.PretId;
import mapping.Support;
import utils.HibernateUtil;

public class PretBoutonListener implements ActionListener {
    List<Oeuvre> oeuvres;
    List<Adherent> adherents;
    MainWindow gui;
    javax.swing.JTable tableO;
    javax.swing.JTable tableA;
    Adherent client;
    Oeuvre oeuvre;
    Pret unPret;
    Session session;
    Support support;

    public PretBoutonListener(Session session, MainWindow gui, ArrayList<Oeuvre> oeuvres, ArrayList<Adherent> adherents) {
        this.gui = gui;
        this.tableO = this.gui.getOeuvreTable();
        this.tableA = this.gui.getAdherendTable();
        this.client = new Adherent();
        this.oeuvre = new Oeuvre();
        this.session = session;
        this.adherents = adherents;
        this.oeuvres = oeuvres;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        if (this.tableO.getSelectedRow() == -1) {
            return;
        }
        if (this.tableA.getSelectedRow() == -1) {
            return;
        }
        client = this.adherents.get(tableA.getSelectedRow());
        oeuvre = this.oeuvres.get(tableO.getSelectedRow());

        System.out.println(client.getPersonne().getPrenom());
        System.out.println(client.getPersonne().getNom());
        System.out.println(oeuvre.getTitre());
        
        Query queryExemplaire = session.createQuery("from Exemplaire");
        ArrayList<Exemplaire> exemplaireList = new ArrayList<>(queryExemplaire.list());
        Exemplaire exemplaire = new Exemplaire();

        for (Exemplaire ex : exemplaireList) {
            if (ex.getNumero().equals(oeuvre.getNumero())) {
                exemplaire = ex;
            }
        }
        
        if("V".equals(exemplaire.getOeuvre().getConsultationseulement())){
            System.out.println("Consultation seulement");
            return;
        }
        
        PretId id = new PretId((BigDecimal) exemplaire.getNumero(), (BigDecimal) client.getNumero(), getDecimalFromDate());
        this.unPret = new Pret(id, exemplaire, client, getDecimalFromDateRetour());
        
        session.save(this.unPret);
        session.getTransaction().commit();
        System.out.println("pret effectué");
        PretPopup pretPop = new PretPopup();
    }

    public BigDecimal getDecimalFromDate() {
        Calendar calendar = Calendar.getInstance();
        java.util.Date now = calendar.getTime();
        return new BigDecimal(now.getTime()/1000);
    }
    
    public BigDecimal getDecimalFromDateRetour() {
        Calendar calendar = Calendar.getInstance();
        java.util.Date now = calendar.getTime();
        return new BigDecimal((now.getTime()/1000)+4);
    }
}
