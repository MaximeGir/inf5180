package mapping;
// Generated 2014-12-14 01:44:17 by Hibernate Tools 4.3.1


import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Exemplaire generated by hbm2java
 */
public class Exemplaire  implements java.io.Serializable {


     private BigDecimal numero;
     private Support support;
     private Oeuvre oeuvre;
     private EtatExemplaire etatExemplaire;
     private Set emplacements = new HashSet(0);
     private Set prets = new HashSet(0);
     private Set reservations = new HashSet(0);

    public Exemplaire() {
    }

	
    public Exemplaire(BigDecimal numero, Support support, Oeuvre oeuvre, EtatExemplaire etatExemplaire) {
        this.numero = numero;
        this.support = support;
        this.oeuvre = oeuvre;
        this.etatExemplaire = etatExemplaire;
    }
    public Exemplaire(BigDecimal numero, Support support, Oeuvre oeuvre, EtatExemplaire etatExemplaire, Set emplacements, Set prets, Set reservations) {
       this.numero = numero;
       this.support = support;
       this.oeuvre = oeuvre;
       this.etatExemplaire = etatExemplaire;
       this.emplacements = emplacements;
       this.prets = prets;
       this.reservations = reservations;
    }
   
    public BigDecimal getNumero() {
        return this.numero;
    }
    
    public void setNumero(BigDecimal numero) {
        this.numero = numero;
    }
    public Support getSupport() {
        return this.support;
    }
    
    public void setSupport(Support support) {
        this.support = support;
    }
    public Oeuvre getOeuvre() {
        return this.oeuvre;
    }
    
    public void setOeuvre(Oeuvre oeuvre) {
        this.oeuvre = oeuvre;
    }
    public EtatExemplaire getEtatExemplaire() {
        return this.etatExemplaire;
    }
    
    public void setEtatExemplaire(EtatExemplaire etatExemplaire) {
        this.etatExemplaire = etatExemplaire;
    }
    public Set getEmplacements() {
        return this.emplacements;
    }
    
    public void setEmplacements(Set emplacements) {
        this.emplacements = emplacements;
    }
    public Set getPrets() {
        return this.prets;
    }
    
    public void setPrets(Set prets) {
        this.prets = prets;
    }
    public Set getReservations() {
        return this.reservations;
    }
    
    public void setReservations(Set reservations) {
        this.reservations = reservations;
    }




}


