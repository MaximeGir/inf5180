package mapping;
// Generated 2014-12-14 01:44:17 by Hibernate Tools 4.3.1



/**
 * Oeuvreauteur generated by hbm2java
 */
public class Oeuvreauteur  implements java.io.Serializable {


     private OeuvreauteurId id;
     private Oeuvre oeuvre;
     private Auteur auteur;

    public Oeuvreauteur() {
    }

    public Oeuvreauteur(OeuvreauteurId id, Oeuvre oeuvre, Auteur auteur) {
       this.id = id;
       this.oeuvre = oeuvre;
       this.auteur = auteur;
    }
   
    public OeuvreauteurId getId() {
        return this.id;
    }
    
    public void setId(OeuvreauteurId id) {
        this.id = id;
    }
    public Oeuvre getOeuvre() {
        return this.oeuvre;
    }
    
    public void setOeuvre(Oeuvre oeuvre) {
        this.oeuvre = oeuvre;
    }
    public Auteur getAuteur() {
        return this.auteur;
    }
    
    public void setAuteur(Auteur auteur) {
        this.auteur = auteur;
    }




}


