package mapping;
// Generated 2014-12-14 01:44:17 by Hibernate Tools 4.3.1



/**
 * Musiqueinterprete generated by hbm2java
 */
public class Musiqueinterprete  implements java.io.Serializable {


     private MusiqueinterpreteId id;
     private Musique musique;
     private Interprete interprete;

    public Musiqueinterprete() {
    }

    public Musiqueinterprete(MusiqueinterpreteId id, Musique musique, Interprete interprete) {
       this.id = id;
       this.musique = musique;
       this.interprete = interprete;
    }
   
    public MusiqueinterpreteId getId() {
        return this.id;
    }
    
    public void setId(MusiqueinterpreteId id) {
        this.id = id;
    }
    public Musique getMusique() {
        return this.musique;
    }
    
    public void setMusique(Musique musique) {
        this.musique = musique;
    }
    public Interprete getInterprete() {
        return this.interprete;
    }
    
    public void setInterprete(Interprete interprete) {
        this.interprete = interprete;
    }




}


