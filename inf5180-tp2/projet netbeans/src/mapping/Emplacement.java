package mapping;
// Generated 2014-12-14 01:44:17 by Hibernate Tools 4.3.1



/**
 * Emplacement generated by hbm2java
 */
public class Emplacement  implements java.io.Serializable {


     private EmplacementId id;
     private Exemplaire exemplaire;

    public Emplacement() {
    }

	
    public Emplacement(EmplacementId id) {
        this.id = id;
    }
    public Emplacement(EmplacementId id, Exemplaire exemplaire) {
       this.id = id;
       this.exemplaire = exemplaire;
    }
   
    public EmplacementId getId() {
        return this.id;
    }
    
    public void setId(EmplacementId id) {
        this.id = id;
    }
    public Exemplaire getExemplaire() {
        return this.exemplaire;
    }
    
    public void setExemplaire(Exemplaire exemplaire) {
        this.exemplaire = exemplaire;
    }




}


