package mapping;
// Generated 2014-12-14 01:44:17 by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Personne generated by hbm2java
 */
public class Personne  implements java.io.Serializable {


     private String identifiant;
     private String nom;
     private String prenom;
     private Set adherents = new HashSet(0);
     private Interprete interprete;
     private Compositeur compositeur;
     private Auteur auteur;
     private Acteur acteur;

    public Personne() {
    }

	
    public Personne(String identifiant, String nom, String prenom) {
        this.identifiant = identifiant;
        this.nom = nom;
        this.prenom = prenom;
    }
    public Personne(String identifiant, String nom, String prenom, Set adherents, Interprete interprete, Compositeur compositeur, Auteur auteur, Acteur acteur) {
       this.identifiant = identifiant;
       this.nom = nom;
       this.prenom = prenom;
       this.adherents = adherents;
       this.interprete = interprete;
       this.compositeur = compositeur;
       this.auteur = auteur;
       this.acteur = acteur;
    }
   
    public String getIdentifiant() {
        return this.identifiant;
    }
    
    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }
    public String getNom() {
        return this.nom;
    }
    
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getPrenom() {
        return this.prenom;
    }
    
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public Set getAdherents() {
        return this.adherents;
    }
    
    public void setAdherents(Set adherents) {
        this.adherents = adherents;
    }
    public Interprete getInterprete() {
        return this.interprete;
    }
    
    public void setInterprete(Interprete interprete) {
        this.interprete = interprete;
    }
    public Compositeur getCompositeur() {
        return this.compositeur;
    }
    
    public void setCompositeur(Compositeur compositeur) {
        this.compositeur = compositeur;
    }
    public Auteur getAuteur() {
        return this.auteur;
    }
    
    public void setAuteur(Auteur auteur) {
        this.auteur = auteur;
    }
    public Acteur getActeur() {
        return this.acteur;
    }
    
    public void setActeur(Acteur acteur) {
        this.acteur = acteur;
    }




}


