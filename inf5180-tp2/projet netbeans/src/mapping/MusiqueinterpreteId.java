package mapping;
// Generated 2014-12-14 01:44:17 by Hibernate Tools 4.3.1


import java.math.BigDecimal;

/**
 * MusiqueinterpreteId generated by hbm2java
 */
public class MusiqueinterpreteId  implements java.io.Serializable {


     private BigDecimal musiquenumero;
     private String interpreteidentifiant;

    public MusiqueinterpreteId() {
    }

    public MusiqueinterpreteId(BigDecimal musiquenumero, String interpreteidentifiant) {
       this.musiquenumero = musiquenumero;
       this.interpreteidentifiant = interpreteidentifiant;
    }
   
    public BigDecimal getMusiquenumero() {
        return this.musiquenumero;
    }
    
    public void setMusiquenumero(BigDecimal musiquenumero) {
        this.musiquenumero = musiquenumero;
    }
    public String getInterpreteidentifiant() {
        return this.interpreteidentifiant;
    }
    
    public void setInterpreteidentifiant(String interpreteidentifiant) {
        this.interpreteidentifiant = interpreteidentifiant;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof MusiqueinterpreteId) ) return false;
		 MusiqueinterpreteId castOther = ( MusiqueinterpreteId ) other; 
         
		 return ( (this.getMusiquenumero()==castOther.getMusiquenumero()) || ( this.getMusiquenumero()!=null && castOther.getMusiquenumero()!=null && this.getMusiquenumero().equals(castOther.getMusiquenumero()) ) )
 && ( (this.getInterpreteidentifiant()==castOther.getInterpreteidentifiant()) || ( this.getInterpreteidentifiant()!=null && castOther.getInterpreteidentifiant()!=null && this.getInterpreteidentifiant().equals(castOther.getInterpreteidentifiant()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getMusiquenumero() == null ? 0 : this.getMusiquenumero().hashCode() );
         result = 37 * result + ( getInterpreteidentifiant() == null ? 0 : this.getInterpreteidentifiant().hashCode() );
         return result;
   }   


}


