
package gui;

import java.math.BigDecimal;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.hibernate.Query;
import org.hibernate.Session;
import mapping.Oeuvre;

public class OeuvreTableModel extends AbstractTableModel {
    
    private final ArrayList<Oeuvre> oeuvreList;
    
    public OeuvreTableModel(Session session){
       super();
       Query query = session.createQuery("from Oeuvre where numero < :code and consultationseulement = :cons");
       query.setParameter("code", new BigDecimal(30));
       query.setParameter("cons", "F");
       this.oeuvreList = new ArrayList<>(query.list());
    }
    
    @Override
    public int getRowCount() {
       return this.getOeuvreList().size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
       Oeuvre oeuvre = getOeuvreList().get(rowIndex);
        Object[] values = new Object[]{
            oeuvre.getNumero(),
            oeuvre.getTitre(),
            oeuvre.getConsultationseulement(),
            oeuvre.getCategorie().getNomcategorie()
        };
        return values[columnIndex];
    }
    
    public Object getSelectedRowFromIndex(int index){
       return this.getOeuvreList().get(index);
    }
    
    @Override
    public String getColumnName(int column)
    {
        String[] columnNames=new String[]{"Numero","Titre","Consultation","Categorie"};
        return columnNames[column];
    }

    /**
     * @return the oeuvreList
     */
    public ArrayList<Oeuvre> getOeuvreList() {
        return oeuvreList;
    }
    
}
