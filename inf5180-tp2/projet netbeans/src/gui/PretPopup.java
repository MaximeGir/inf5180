
package gui;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PretPopup {
  final JFrame setting = new JFrame();
        JPanel container = new JPanel();
        JPanel bouton = new JPanel();
        JPanel pan = new JPanel();

        JLabel confirmer = new JLabel("Pret effectué avec succès!");
        JButton bouton1 = new JButton("Ok");

 public PretPopup(){
        setting.setTitle("Confirmation de pret");
        setting.setSize(300, 150);
        setting.setLocationRelativeTo(null);

        pan.add(confirmer);
        bouton.add(bouton1);

        container.setBackground(Color.white);
        container.setLayout(new BorderLayout());
        container.add(pan, BorderLayout.CENTER);
        container.add(bouton, BorderLayout.SOUTH);

        setting.setContentPane(container);
        setting.setVisible(true);

        bouton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setting.setVisible(false);
            }
        });
   }
}
