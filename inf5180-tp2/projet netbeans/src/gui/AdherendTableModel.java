
package gui;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import org.hibernate.Query;
import org.hibernate.Session;
import mapping.Adherent;

public class AdherendTableModel extends AbstractTableModel {

    private ArrayList<Adherent> adherentList;
    
    public AdherendTableModel(Session session){
      super();
      Query query = session.createQuery("from Adherent");
      this.adherentList = new ArrayList<>(query.list());
    }
    
    @Override
    public int getRowCount() {
       return this.getAdherentList().size();
    }

    @Override
    public int getColumnCount() {
       return 4;
    }


    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
       Adherent adherent = this.getAdherentList().get(rowIndex);
        Object[] values = new Object[]{
            adherent.getNumero(),
            adherent.getPersonne().getNom(),
            adherent.getTelephone(),
            adherent.getAdresse()
        };
        return values[columnIndex];
    }
    
    @Override
    public String getColumnName(int column)
    {
        String[] columnNames=new String[]{"Numero", "Nom", "Telephone", "Adresse"};
        return columnNames[column];
    }

    /**
     * @return the adherentList
     */
    public ArrayList<Adherent> getAdherentList() {
        return adherentList;
    }
}
